# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

from argparse import ArgumentParser

from powders.filename import list_hyst
from powders.filename import SortBy
from powders.slab import DataSlab


def create_argument_parser():
    r"""
    Create a command line argument parser.
    :return: a command line argument parser.
    """
    parser = ArgumentParser()

    parser.add_argument("data_dir", help="directory holding hysteresis files")
    parser.add_argument("ngrains", type=int, help="the number of grains")

    return parser


def main():
    parser = create_argument_parser()
    args = parser.parse_args()

    hyst_files = list_hyst(args.data_dir, sort_by=SortBy.SIZE, ngrains_filter=args.ngrains)

    data_slab = DataSlab(hyst_files)

    for line in data_slab.dir_averaged_data_frames[50]:
        print("{},{}".format(*line))



# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()
