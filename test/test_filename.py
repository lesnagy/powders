r"""
Test the 'filename.py' utilities.
"""
import os
import shutil
import unittest

from powders.filename import HysteresisFile
from powders.filename import SortBy
from powders.filename import list_hyst


class TestListHyst(unittest.TestCase):
    def setUp(self):
        # Create a temporary directory and put some files in it.
        self.test_data_dir = "test_data_dir"

        file_names = [
            "Hyst_Easy_110nmx4_mult.tec",
            "Mrs_Hard100_90nmx1.hyst",
            "Hyst_Hard_350nmx4_mult.tec",
            "Hyst_Hard_80nmx3_mult.tec",
            "150nmx4.pat",
            "Mrs_Hard_120nmx3_mult.tec",
            "Mrs_Hard_25nmx3_mult.tec",
            "Hyst_Easy_150nmx3.hyst",
            "Mrs_Easy_160nmx4.hyst",
            "Hyst_Easy_25nmx4.hyst",
            "50nmx6.pat",
            "Mrs_Easy_120nmx1_mult.tec",
            "Hyst_Hard_110nmx4_mult.tec",
            "Hyst_Hard_250nmx2.hyst",
            "Mrs_Easy_120nmx1.hyst",
            "Hyst_Hard_110nmx4.hyst",
            "Mrs_Easy_160nmx3_mult.tec",
            "110nmx6.out",
            "Hyst_Easy_70nmx6.hyst",
            "Mrs_Hard100_200nmx1.hyst",
            "300nmx6.out",
            "Mrs_Easy_70nmx2.hyst",
            "140nmx1.merrill",
            "Mrs_Hard_60nmx2_mult.tec",
            "250nmx5.merrill",
            "Hyst_Hard_400nmx2_mult.tec",
            "Hyst_Easy_400nmx2.hyst",
            "Hyst_Easy_40nmx3.hyst",
            "Mrs_Easy_100nmx5_mult.tec",
        ]

        os.mkdir("test_data_dir")

        for file_name in file_names:
            with open(os.path.join(self.test_data_dir, file_name), "w") as fout:
                fout.write("blank")

    def tearDown(self):
        # Remove temporary directory.
        shutil.rmtree(self.test_data_dir)

    def test_find_all_hyst(self):
        hfiles = list_hyst(self.test_data_dir, sort_by=SortBy.FILE_NAME)

        self.assertEqual(len(hfiles), 7)

        self.assertEqual(hfiles[0],
                         HysteresisFile("Easy", 150, 3, "Hyst_Easy_150nmx3.hyst",
                                        "test_data_dir\Hyst_Easy_150nmx3.hyst"))
        self.assertEqual(hfiles[1],
                         HysteresisFile("Easy", 25, 4, "Hyst_Easy_25nmx4.hyst", "test_data_dir\Hyst_Easy_25nmx4.hyst"))
        self.assertEqual(hfiles[2], HysteresisFile("Easy", 400, 2, "Hyst_Easy_400nmx2.hyst",
                                                   "test_data_dir\Hyst_Easy_400nmx2.hyst"))
        self.assertEqual(hfiles[3],
                         HysteresisFile("Easy", 40, 3, "Hyst_Easy_40nmx3.hyst", "test_data_dir\Hyst_Easy_40nmx3.hyst"))
        self.assertEqual(hfiles[4],
                         HysteresisFile("Easy", 70, 6, "Hyst_Easy_70nmx6.hyst", "test_data_dir\Hyst_Easy_70nmx6.hyst"))
        self.assertEqual(hfiles[5], HysteresisFile("Hard", 110, 4, "Hyst_Hard_110nmx4.hyst",
                                                   "test_data_dir\Hyst_Hard_110nmx4.hyst"))
        self.assertEqual(hfiles[6], HysteresisFile("Hard", 250, 2, "Hyst_Hard_250nmx2.hyst",
                                                   "test_data_dir\Hyst_Hard_250nmx2.hyst"))

    def test_filter_1(self):
        hfiles = list_hyst(self.test_data_dir, sort_by=SortBy.FILE_NAME, size_filter=25)

        self.assertEqual(len(hfiles), 1)

        self.assertEqual(hfiles[0],
                         HysteresisFile("Easy", 25, 4, "Hyst_Easy_25nmx4.hyst", "test_data_dir\Hyst_Easy_25nmx4.hyst"))

    def test_filter_2(self):
        hfiles = list_hyst(self.test_data_dir, sort_by=SortBy.FILE_NAME, direction_filter="Easy", ngrains_filter=4)

        self.assertEqual(len(hfiles), 1)

        self.assertEqual(hfiles[0],
                         HysteresisFile("Easy", 25, 4, "Hyst_Easy_25nmx4.hyst",
                                        "test_data_dir\Hyst_Easy_25nmx4.hyst"))


if __name__ == '__main__':
    unittest.main()
