import math
from decimal import Decimal
from typing import List

import numpy as np
import scipy.interpolate
import scipy.optimize as sciop
from powders.data_io import read_csv_hyst
from powders.filename import HysteresisFile
from scipy.integrate import quad
from scipy.stats import lognorm


def is_near(array1, array2, eps=1E-10):
    r"""
    Check that two arrays are point-wise near to each other.
    :param array1: the first array.
    :param array2: the second array.
    :param eps: epsilon (nearness) value.
    :return: True if array1 and array2 are near otherwise false.
    """
    if len(array1) != len(array2):
        return False

    for v1, v2 in zip(array1, array2):
        if eps < abs(v1 - v2):
            return False

    return True


def lognorm_dist(mean_size):
    r"""
    Creates a lognormal distribution with medium value = size, and standard deviation = size/3.
    :return: a scipy lognormal object.
    """
    variance = (mean_size / 3) ** 2
    scale = mean_size / (variance / (mean_size ** 2) + 1) ** .5
    shape = 0.35
    nd = lognorm(shape, loc=0, scale=scale).pdf
    return nd


class DirectionDataFrame:
    def __init__(self, direction, data_frame):
        self.direction = direction
        self.data_frame = data_frame


class SizeVsRemanenceInterpolationFunction:
    def __init__(self, Xs, Ys, size_extrap, size_from):
        # x - values beyond or equal to `extrapolation_size` are used for the extrapolation region.
        self.interpolation = scipy.interpolate.interp1d(Xs, Ys,
                                                        fill_value="extrapolate",
                                                        kind="linear")
        self.size_extrap = size_extrap
        self.size_from = size_from

        data = zip(Xs, Ys)
        data_ex = [p for p in data if p[0] >= self.size_extrap]
        Xps = [p[0] for p in data_ex]
        Yps = [p[1] for p in data_ex]

        #print(Xps)
        #print(Yps)

        self.n = len(Xps)

        def func(x, a, b, c):
            return a * x ** b + c

        popt, pcov = sciop.curve_fit(func, Xps, Yps, maxfev=100000)

        self.a = popt[0]
        self.b = popt[1]
        self.c = popt[2]

    def get_n(self):
        return self.n

    def __call__(self, size, *args, **kwargs):
        if size < self.size_from:
            return self.interpolation(size)
        else:
            return self.a * size ** self.b + self.c


class DataSlab:
    def __init__(self, hyst_file_data: List[HysteresisFile], extrapolation_size,extrapolate_from):
        self.extrapolate_from = extrapolate_from
        self.hyst_file_data = hyst_file_data
        self.extrapolation_size = extrapolation_size

        # A collection of all data frames by size, each size has any no. of directions.
        self.all_dir_data_frames = {}

        # A collection of data frames that have been averaged over directions by size/field.
        self.dir_averaged_data_frames = {}

        # Direction-averaged data restructured by field/size.
        self.dir_averaged_data_frames_by_field = {}

        # Interpolations.
        self.dir_averaged_interpolations_by_field = {}

        # interperlations minimums used for log-log offset
        self.mr_ms_mins = {}

        self.read_data()
        self.average_over_direction()
        self.rearrange_average_by_field()
        self.calculate_dir_averaged_interpolations_by_field()

        self.extrapolation_size = extrapolation_size

        # print(self.all_dir_data_frames)

    def read_data(self):
        r"""
        Read in raw hysteresis data with size as key and data frames over directions.
        :return: None
        """
        for hyst_file in self.hyst_file_data:

            # print(hyst_file.size)
            # print(self.all_dir_data_frames)
            if hyst_file.size not in self.all_dir_data_frames.keys():
                self.all_dir_data_frames[hyst_file.size] = []
                # print("set")
                # print(self.all_dir_data_frames[hyst_file.size])
                # print("done")

            self.all_dir_data_frames[hyst_file.size].append(
                DirectionDataFrame(
                    hyst_file.direction,
                    read_csv_hyst(hyst_file.full_path)
                )
            )

    def average_over_direction(self):
        r"""
        Average hysteresis data over directions.
        :return: None
        """
        for key, dir_data_frames in self.all_dir_data_frames.items():
            # dir_data_frames is a list containing dataframes belonging to the key, i.e grain size
            # and here creates data frame (.i.e.dir_averaged_data_frames) of hysteresis [field, mag]
            # for each grain size
            n = float(len(dir_data_frames))

            # NOTE: Use "mr" below for absolute magnetization values, otherwise "mr_ms"
            avg_mr_ms = sum([dir_data_frame.data_frame[["applied_field", "mr"]].values for dir_data_frame in
                             dir_data_frames]) / n
            self.dir_averaged_data_frames[key] = avg_mr_ms

        # for key in self.dir_averaged_data_frames:
        #     print(key,'-->', self.dir_averaged_data_frames[key])

    def sizes(self):
        r"""
        Return a list of available sizes.
        :return: a list of sizes.
        """
        return list(self.all_dir_data_frames.keys())

    def field_values(self):
        r"""
        Return a coherent list of field values.
        :return:
        """
        initial_field_values = None
        for key, dir_data_frames in self.all_dir_data_frames.items():
            if initial_field_values is None:
                initial_field_values = [col[0] for col in dir_data_frames[0].data_frame[["applied_field"]].values]
                # print(initial_field_values)
            # Check for equality

            for dir_data_frame in dir_data_frames:
                field_values = [col[0] for col in dir_data_frame.data_frame[["applied_field"]].values]
                if not is_near(initial_field_values, field_values):
                    return None

        initial_field_values = [Decimal("{:5.3f}".format(value)) for value in initial_field_values]
        # print(initial_field_values)
        return initial_field_values

    def rearrange_average_by_field(self):
        r"""
        :return:
        """
        field_values = self.field_values()
        sizes = self.dir_averaged_data_frames.keys()

        # Create an empty list
        for field in field_values:
            self.dir_averaged_data_frames_by_field[field] = {}
            for size in sizes:
                self.dir_averaged_data_frames_by_field[field][size] = 0.0

        for size in sizes:
            dir_avg_data = self.dir_averaged_data_frames[size]
            for row in dir_avg_data:
                field = Decimal("{:5.3f}".format(row[0]))
                mr_ms = row[1]
                self.dir_averaged_data_frames_by_field[field][size] = mr_ms
        # print(self.dir_averaged_data_frames_by_field)

    def calculate_dir_averaged_interpolations_by_field(self):
        r"""
        :return:
        """
        # print(self.dir_averaged_data_frames_by_field.items())
        for field, data in self.dir_averaged_data_frames_by_field.items():
            #mr_ms_min = min([mr_ms for size, mr_ms in data.items()])
            dss = [(size, mr_ms) for size, mr_ms in data.items()]
            # print(field, dss)
            x = [v[0] for v in dss]
            y = [v[1] for v in dss]
            # y = [(lambda sm:0 if sm[0] < 0 else sm[1]) (v) for v in dss]

            # self.dir_averaged_interpolations_by_field[field] = scipy.interpolate.interp1d(x, y,
            #                                                                              fill_value="extrapolate",
            #                                                                              kind="linear")

            self.dir_averaged_interpolations_by_field[field] = SizeVsRemanenceInterpolationFunction(
                x, y, self.extrapolation_size, self.extrapolate_from
            )

            #self.mr_ms_mins[field] = mr_ms_min

    def loop_at(self, size):
        field_values = self.field_values()
        # print('size = ', size)
        # print(field_values)
        loop = []
        for field in field_values:
            ifun = self.dir_averaged_interpolations_by_field[field]
            loop.append((float(field), float(ifun(size))))
        return loop

    def loop_original_at(self, size):
        field_values = self.field_values()
        # print('size = ', size)
        # print(field_values)
        loop = []
        for field in field_values:
            ifun = self.dir_averaged_data_frames_by_field[field][size]
            # print(ifun)
            # print('ifun', ifun(size))
            loop.append((float(field), float(ifun(size))))
        return loop

    def loop_and_integrate_at(self, mean_size):
        field_values = self.field_values()
        # print(field_values)
        lower = 25.
        upper = 5000.
        loop = []
        value = 0.
        ifun = self.dir_averaged_interpolations_by_field[field_values[0]]
        pdf = lognorm_dist(mean_size)
        value2 = quad(lambda size_var: pdf(size_var) * ifun(size_var), 25., upper, limit=5000)[0]
        for field in field_values:
            value = 0.
            ifun = self.dir_averaged_interpolations_by_field[field]
            # print(lognorm_dist(size))
            value = quad(lambda size_var: pdf(size_var) * ifun(size_var), lower, upper, limit=5000)[0]
            loop.append((float(field), float(value), float(value2)))

        return loop
