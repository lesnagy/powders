import pandas as pd


def read_csv_hyst(file, skiprows=1):
    r"""
    Reads the hysteresis data from file (in csv) in to a Pandas Dataframe.
    :param file: the name of a CSV formatted file containing hysteresis data.
    :return: a Pandas data frame.
    """
    data_frame = pd.read_csv(file, nrows=256, skiprows=skiprows)
    data_frame.columns = ["applied_field", "mr", "mr_ms", "mx", "my", "mz"]

    return data_frame
