import os
import re

from enum import Enum

class HysteresisFile:
    r"""
    Class to hold information about a Hysteresis file.
    """

    def __init__(self, direction: str, size: int, ngrains: int, file_name: str, full_path: str):
        self.direction = direction
        self.size = size
        self.ngrains = ngrains
        self.file_name = file_name
        self.full_path = full_path

    def __eq__(self, other):
        return self.direction == other.direction \
               and self.size == other.size \
               and self.ngrains == other.ngrains \
               and self.file_name == other.file_name \
               and self.full_path == other.full_path

    def __str__(self):
        str_components = [
            "direction: {}".format(self.direction),
            "size:      {}".format(self.size),
            "ngrains:   {}".format(self.ngrains),
            "file_name: {}".format(self.file_name),
            "full_path: {}".format(self.full_path),
        ]
        return "\n".join(str_components)


class SortBy(Enum):
    DIRECTION = 1
    SIZE = 2
    NGRAINS = 3
    FILE_NAME = 4
    FULL_PATH = 5

def list_hyst(dir_name, sort_by=None, direction_filter=None, size_filter=None, ngrains_filter=None):
    r"""
    Will list all the hysteresis files in the input directory. These files have the format
    'Hyst_<dir>_<size>nmx<ngrain>.hyst' where
        <dir>     is the direction in which the field is applied
        <size>    is the size of each individual grain
        <ngrains> is the number of grains
    :param dir_name: the directory containing hysteresis files.
    :param sort_by: the enumeration field value to sort by, default = None.
    :param direction_filter: if given a value, filter the output by this direction.
    :param size_filter: if given a value, filter the output by this size.
    :param ngrains_filter: if given a value, filter the output by this number of grains.
    :return: a list of.
    """
    regex_hyst_file = re.compile(r"Hyst_((Hard)|(Easy))_([0-9]+)nmx([0-9]+)\.hyst")
    #regex_hyst_file = re.compile(r"Hyst_((Hard)|(Easy))_Trelis([0-9]+)\.hyst")

    hyst_file_data = []
    for file_name in os.listdir(dir_name):
        match_hyst_file = regex_hyst_file.match(file_name)
        if match_hyst_file:
            hyst_file_data.append(
                HysteresisFile(
                    match_hyst_file.group(1),
                    int(match_hyst_file.group(4)),
                    int(match_hyst_file.group(5)),
                    file_name,
                    os.path.relpath((os.path.join(dir_name, file_name)))
                )
            )
#            print(HysteresisFile)
#     for filevals in hyst_file_data:
#         print(filevals)

    if sort_by is not None:
        if sort_by == SortBy.DIRECTION:
            hyst_file_data.sort(key=lambda hfile: hfile.direction)
        elif sort_by == SortBy.SIZE:
            hyst_file_data.sort(key=lambda hfile: hfile.size)
        elif sort_by == SortBy.NGRAINS:
            hyst_file_data.sort(key=lambda hfile: hfile.ngrains)
        elif sort_by == SortBy.FILE_NAME:
            hyst_file_data.sort(key=lambda hfile: hfile.file_name)
        elif sort_by == SortBy.FULL_PATH:
            hyst_file_data.sort(key=lambda hfile: hfile.full_path)
        else:
            raise ValueError("Unknown value for parameter 'sort_by'")

    if direction_filter is not None:
        hyst_file_data = filter(lambda hfile: hfile.direction == direction_filter, hyst_file_data)

    if size_filter is not None:
        hyst_file_data = filter(lambda hfile: hfile.size == size_filter, hyst_file_data)

    if ngrains_filter is not None:
        hyst_file_data = filter(lambda hfile: hfile.ngrains == ngrains_filter, hyst_file_data)
    return list(hyst_file_data)
