from argparse import ArgumentParser
from decimal import Decimal

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from powders.filename import SortBy
from powders.filename import list_hyst
from powders.slab import DataSlab
from scipy.stats import lognorm


def create_argument_parser():
    r"""
    Create a command line argument parser.
    :return: a command line argument parser.
    """
    parser = ArgumentParser()

    parser.add_argument("data_dir", help="directory holding hysteresis files")
    parser.add_argument("ngrains", type=int, help="the number of grains")
    parser.add_argument("field", type=float, help="the field over which to plot sizes")
    parser.add_argument("--xmin", type=float, default=None)
    parser.add_argument("--xmax", type=float, default=None)
    parser.add_argument("--ymin", type=float, default=None)
    parser.add_argument("--ymax", type=float, default=None)

    return parser


def sigmoid(x, L, x0, k, b):
    # define a sigmoid fucntion to fit to hysteresis loops - not used
    y = L / (1 + np.exp(-k * (x - x0))) + b
    return (y)


def lognorm_dist(mean_size):
    r"""
    Creates a lognormal distribution with medium value = size, and standard deviation = size/3.
    :return: a scipy lognormal object.
    """
    variance = (mean_size / 3) ** 2
    scale = mean_size / (variance / (mean_size ** 2) + 1) ** .5
    shape = 0.35
    nd = lognorm(shape, loc=0, scale=scale)
    return nd


def main():
    parser = create_argument_parser()
    args = parser.parse_args()
    size_range = np.arange(0, 1000, 0.1)
    pdf = lognorm_dist(20).pdf
    lognorm_vals = [pdf(size) for size in size_range]

    plt.plot(size_range, lognorm_vals, label="lognorm")
    plt.show()

    # print(args)
    hyst_files = list_hyst(args.data_dir, sort_by=SortBy.SIZE, ngrains_filter=args.ngrains)
    # for ystfile in hyst_files:
    #   print (ystfile)

    data_slab = DataSlab(hyst_files, 40, 250)

    field_values = data_slab.field_values()
    #   print(field_values)

    field_value = Decimal("{:5.3}".format(args.field))

    field_deci = Decimal("{:5.3f}".format(field_value))

    mr_mss = [(key, value) for key, value in data_slab.dir_averaged_data_frames_by_field[field_deci].items()]

    size = [v[0] for v in mr_mss]
    mr_ms = [v[1] for v in mr_mss]

    # plt.plot(size, mr_ms, label=field_deci)

    size_min = min(size)
    size_max = max(size)

    ifun = data_slab.dir_averaged_interpolations_by_field[field_deci]
    isize = np.arange(size_min, size_max + 300, 0.1)
    imr_ms = [ifun(x) for x in isize]

    # newdata=data_slab.dir_averaged_data_frames
    # print(newdata.keys())
    # original_sizes=newdata.keys()
    # print(original_sizes)
    # newdatavals={}
    # for key, newdatavals in newdata.items():
    #    print(key,'-->', newdatavals)

    size_range = np.arange(30, 900, 10)
    # use the given data sizes below for averaging raw data only (no interp)
    # size_range = data_slab.dir_averaged_data_frames.keys()

    #   quit()
    size_hyst=[]

    do_integration = False
    if do_integration:

        for grain_size in size_range:

            loop = data_slab.loop_and_integrate_at(grain_size)

            # loop = data_slab.loop_at(grain_size)
            # loop = data_slab.loop_original_at(grain_size)
            # print(loop)
            # use maxmr for single size - i.e. no integration with SP cut off
            # use max_norm for distribution of sizes - i.e. to account for integration with SP cut off

            # maxmr=max(loop, key=lambda item: item[1])[1]
            #max_norm=max(loop, key=lambda item: item[2])[2]
            max_norm = loop[0][1]
            applied_field = [v[0] for v in loop]

            # CAREFUL : Use max_norm or maxmr
            mr_ms = [v[1]/max_norm for v in loop]

            Hc_index= np.where(np.diff(np.sign(mr_ms)))[0]
            print("Hc Index", Hc_index)
            idx1=Hc_index[0]
            idx2=idx1+1
            print("Mrs vals",mr_ms[idx1], mr_ms[idx2])

            gradient=(mr_ms[idx1] - mr_ms[idx2])/(applied_field[idx1] - applied_field[idx2])
            intercept= mr_ms[idx1] - gradient*applied_field[idx1]
        #Hc_val=(applied_field[Hc_index[0]] + applied_field[Hc_index[0] + 1])/2
            Hc_val= -intercept/gradient
            Mrs_index= np.where(np.diff(np.sign(applied_field)))[0]
        # print("Mrs index", Mrs_index)
            Mrs_val=mr_ms[Mrs_index[0] + 1]

        #print("Grain size", grain_size/1000,"Hc = ", Hc_val,"Mrs = ", Mrs_val )
            print('GRAIN SIZE =', grain_size)
            print("Hc = ", Hc_val)
            print("Mrs = ", Mrs_val, "at field", applied_field[Mrs_index[0] + 1] )

            size_hyst.append((float(grain_size/1000.), float(Mrs_val), float(-1000*Hc_val)))



    #outfile = '/Users/williams/Desktop/TRELIS37'
    outfile = "D:\\data\\powders2\\TRELIS37"

    if do_integration:
        with open(outfile+'.txt' ,'w') as fout:
            fout.write('{:<15},{:<15},{:<15}\n'.format('Size', 'Mrs', 'Hc'))
            for t in size_hyst:
               fout.write('{:<15.5f},{:<15.5f},{:<15.5f}\n'.format(t[0],t[1],t[2]))


    #  create empty panda file
    sizelist = [30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 160, 180, 200, 225, 250, 270, 300, 350, 400, 450, 500, 600,
                700, 800, 900]
    #sizelist = [60]
    # sizelist = data_slab.dir_averaged_data_frames.keys()
    hyst_table = pd.DataFrame()
    hyst_table['Field Vals'] = field_values

    for sizer in sizelist:
        if do_integration:
            loop = data_slab.loop_and_integrate_at(sizer)
        # norm=max(loop, key=lambda item: item[2])[2]
        else:
            loop = data_slab.loop_at(sizer)
        # loop = data_slab.loop_original_at(sizer)
        norm = max(loop, key=lambda item: item[1])[1]
        applied_field = [v[0] for v in loop]
        mr_ms = [v[1] / norm for v in loop]
        hyst_table[sizer] = mr_ms
        plt.plot(applied_field, mr_ms, label="{}".format(sizer))
    hyst_table.to_csv(outfile + '.hyst', index=False)

    # for field in np.arange(0.02, -0.025, -0.005):
    #     field_deci = Decimal("{:5.3f}".format(field))
    #     print(field_deci)
    #
    #     mr_mss = [(key, value) for key, value in data_slab.dir_averaged_data_frames_by_field[field_deci].items()]
    #
    #     size = [v[0] for v in mr_mss]
    #     mr_ms = [v[1] for v in mr_mss]
    #
    #     plt.plot(size, mr_ms, label=field_deci)

    if args.xmin is not None and args.xmax is not None:
        plt.xlim([args.xmin, args.xmax])

    if args.ymin is not None and args.ymax is not None:
        plt.ylim([args.ymin, args.ymax])

    plt.axhline(y=0)
    plt.axvline(x=0)
    axes = plt.gca()
    axes.set_xlim([-.5, .5])
    axes.set_ylim([-1, 1.])
    plt.title("TRELIS 13")
    plt.legend(loc="lower right")
    plt.show()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()
